#!/usr/bin/env bash

set -eu

is_arm() { [[ "${UNAME}" = "arm64" ]]; }

UNAME="$(uname -m)"

DOTPATH="${HOME}/Developer/git/bitbucket/fujiyoshi-masaaki/environment/dotfiles"
SCRIPT='/bin/bash -c "$(curl -L https://bitbucket.org/fujiyoshi-masaaki/dotfiles/raw/HEAD/bootstrap.sh)"'

xcode-select -p 1>/dev/null || {
  echo 'Installing Command line tools ...'
  xcode-select --install
  is_arm && {
    echo 'Installing Rosetta2 ...'
    /usr/sbin/softwareupdate --install-rosetta --agree-to-license
  }
  echo 'command-line-tools and Rosetta2 (M1 Mac only) have been installed.'
  echo "Please exec ${SCRIPT} again."
  exit 1
}

[[ "${UNAME}-$(arch -arm64 uname -m)" = "x86_64-arm64" ]] && {
  echo 'This script can not be executed on Rosetta2 terminals.'
  echo "Please exec ./bootstrap.sh in ${DOTPATH} on a non-Rosetta2 terminal."
  exit 1
}

if [[ ! -d "${DOTPATH}" ]]
then
  echo 'Cloning dotfiles.git ...'
  git clone https://fujiyoshi-masaaki@bitbucket.org/fujiyoshi-masaaki/dotfiles.git "${DOTPATH}"
else
  echo "${DOTPATH} has been already downloaded."
  [[ "X$*" = "Xupdate" ]] && {
    echo 'Updating dotfiles.git ...'
    cd "${DOTPATH}"
    git stash
    git checkout master
    git pull origin master
    echo
  }
fi

cd "${DOTPATH}"

echo 'Deploying dotfiles ...'
for file in .??*
do
  [[ "${file}" = ".git" ]] && continue
  [[ "${file}" = ".DS_Store" ]] && continue
  ln -fvns "${DOTPATH}/${file}" "${HOME}/${file}"
done

# install homebrew
command -v brew >/dev/null 2>&1 || {
  # Install homebrew for Intel Mac or for Rosetta2 on M1 Mac
  echo 'Installing homebrew in /usr/local for Intel or Rosetta2 ...'
  arch -x86_64 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  echo
  is_arm && {
    echo 'Installing homebrew in /opt/homebrew for Arm ...'
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    cd /usr/local/bin
    ln -sf /usr/local/Homebrew/bin/brew brew_x86
    cd "${DOTPATH}"
  }
}

if is_arm
then
  PATH="$(echo -n ${PATH} | tr ':' '\n' | sed '/\/opt\/homebrew\/bin/d' | tr '\n' ':')"
  echo "${PATH}"

  echo 'brew bundle on Rosetta2 ...'
  cd rosetta2_brew
  arch -x86_64 brew_x86 bundle -v

  PATH="/opt/homebrew/bin:${PATH}"
  echo "${PATH}"
  echo 'brew bundle on Arm native ...'
  cd ../arm_brew
  brew bundle -v
  cd ..
else
  echo 'brew bundle on Intel ...'
  cd intel_brew
  brew bundle -v
  cd ..
fi
echo

eval "$(/usr/libexec/path_helper)"
if is_arm
then
  eval "$(/opt/homebrew/bin/brew shellenv)"
else
  eval "$(/usr/local/bin/brew shellenv)"
fi

defaults write com.apple.dock ResetLaunchPad -bool true
killall Dock

command -v tlmgr >/dev/null 2>&1 && {
  echo 'Updating MacTeX (TeX Live) components ...'
  sudo tlmgr update --self --all --no-persistent-downloads --reinstall-forcibly-removed
  echo
}

command -v mackup >/dev/null 2>&1 && {
  echo 'Restoring config files by mackup ...'
  mackup restore
  echo
}

echo 'Bootstrapping DONE!'
